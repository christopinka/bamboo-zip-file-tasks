package com.pronetbeans.bamboo.filezip;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.*;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.zip.ZipOutputStream;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

/**
 * Bamboo Task for zipping files.
 *
 * @author Adam Myatt
 */
public class FileCreateZipTask implements TaskType {

    private static final Logger log = Logger.getLogger(FileCreateZipTask.class);

    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException {

        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.create(taskContext);
        final BuildLogger buildLogger = taskContext.getBuildLogger();

        final String propsZipLocation = taskContext.getConfigurationMap().get("propsZipLocation");
        final String propsFilePattern = taskContext.getConfigurationMap().get("propsFilePattern");
        final String compressionLevel = taskContext.getConfigurationMap().get("compressionLevel");
        int compressionLevelNum = 0;
        boolean itWorked = false;
        ZipOutputStream out = null;

        try {
            compressionLevelNum = Integer.parseInt(compressionLevel.substring(0, 1));

            // if value somehow or maliciously outside the range 0-9 then reset to zero for safety
            if (compressionLevelNum > 9 || compressionLevelNum < 0) {
                compressionLevelNum = 0;
            }

        } catch (Exception e) {
            compressionLevelNum = 0;
        }

        try {
            out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(new File(taskContext.getWorkingDirectory(), propsZipLocation))));
            buildLogger.addBuildLogEntry("Setting Zip Compression Level To : " + compressionLevelNum);
            out.setLevel(compressionLevelNum);

            ZipFileVisitor visitor = new ZipFileVisitor(taskContext.getWorkingDirectory());
            visitor.setBuildLogger(buildLogger);
            visitor.setOut(out);
            visitor.setPropsZipLocation(propsZipLocation);

            try {
                visitor.visitFilesThatMatch(propsFilePattern);
                itWorked = true;

            } catch (InterruptedException e) {
                taskResultBuilder.failedWithError().build();
                itWorked = false;
            }

        } catch (Exception e) {
            log.info(Utils.getLogBanner());
            buildLogger.addErrorLogEntry("Error generating zip file : " + e.getMessage(), e);
        } finally {

            try {
                if (out != null) {
                    out.close();
                    out = null;
                }
            } catch (Exception e) {
                log.info(Utils.getLogBanner());
                buildLogger.addErrorLogEntry("Error generating zip file : " + e.getMessage(), e);
            }

        }

        if (itWorked) {
            return TaskResultBuilder.create(taskContext).success().build();
        } else {
            return TaskResultBuilder.create(taskContext).failedWithError().build();
        }
    }
}